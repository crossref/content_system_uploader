# Content System Submission Uploader

The upload tool submits content registration and query files to the Crossref content system.

## Install

Build the jar using Maven and place it in 

    /usr/local/lib/content_system_uploader-VERSION.jar

Create the helper script, eg /usr/local/bin/upload

    #!/bin/bash
    java -jar /usr/local/lib/content_system_uploader-VERSION.jar "$@"

You can now use the script so instead of this

    java -jar content_system_uploader-VERSION.jar --user USER PASSWORD --metadata deposit.xml 

You can do this:

    upload --user USER PASSWORD --metadata deposit.xml 

## Usage

In the following examples the **USER** and **PASSWORD** is a system username and password, respectively, **FILE** is the name of the file you are submitting, and **DIRECTORY** is the name of the directory containing one or more files to submit. 

Note that the Crossref system allows searching for submissions by file name and so it is beneficial to use unique file names.

To upload metadata deposits use

    upload --user USER PASSWORD --metadata ( FILE | DIRECTORY )

Eg,

    upload --user USER PASSWORD --metadata deposit.xml 
    upload --user USER PASSWORD --metadata deposits/
    
To upload resource updates use

    upload --user USER PASSWORD --resources ( FILE | DIRECTORY )
    
Eg,

    upload --user USER PASSWORD --resources resources.xml
    upload --user USER PASSWORD --resources resources/

To upload URL updates / ownership transfers use


    upload --user USER PASSWORD --transfers ( FILE | DIRECTORY )

Eg,

    upload --user USER PASSWORD --transfers transfers.txt
    upload --user USER PASSWORD --transfers transfers/
    
To upload conflict resolutions use

    upload --user USER PASSWORD --conflicts ( FILE | DIRECTORY )

Eg,

	upload --user USER PASSWORD --conflicts conflicts.txt
	upload --user USER PASSWORD --conflicts conflicts/

To direct upload(s) to the test system specify the host name. Eg,

    upload --user USER PASSWORD --host test.crossref.org --metadata deposit.xml

### Dry run

When any of the content type options is given a directory name (instead of a file name) all files within the directory are uploaded. To ensure that you are uploading what you expect, first perform a "dry run" to review the set of files to be uploaded to Crossref. Eg,

    upload --user USER PASSWORD --metadata mydeposits/ --dry-run

### Actual Run

If your upload is successful you will see this message:

    [...] INFO uploading to https://doi.crossref.org:443/
    [...] INFO uploading submission: file=myfile.xml
    [...] INFO uploaded submission: file=myfile.xml
    [...] INFO done
    
If the login is wrong then you will see the message:

    [...] INFO uploading to https://doi.crossref.org:443/
    [...] INFO uploading submission: file=myfile.xml
    [...] INFO unauthorized: file=myfile.xml; user=myaccount
    [...] INFO done
    
### All Options

    --user name password
    
Your Crossref system username and password
    
    --metadata ( FILE | DIRECTORY )
    
Use with metadata deposit XML files
    
    --query ( FILE | DIRECTORY )
    
Use with query XML files
        
    --resources ( FILE | DIRECTORY )
    
Use with resource update text files
    
    --conflicts ( FILE | DIRECTORY ) 
    
Use with conflict resolution text files
    
    --transfers ( FILE | DIRECTORY )
    
Administrative only use with title transfer text files
    
    --handles ( FILE | DIRECTORY )

Administrative only use with Handle update text files

    --dry-run
    
Use to review what will be uploaded.
    
    --host domain-name
    
Use to direct uploads to a different address, eg test.crossref.org

    --help
    
Displays the above list of upload options
    
END