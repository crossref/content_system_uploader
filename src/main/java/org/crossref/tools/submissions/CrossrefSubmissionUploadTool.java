package org.crossref.tools.submissions;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.commons.httpclient.methods.multipart.StringPart;

public class CrossrefSubmissionUploadTool {

    private static final SimpleDateFormat LOG_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSZ");

    private enum Level {
        INFO, WARNING, ERROR
    };

    private String usage = String.format("usage: %s \n", CrossrefSubmissionUploadTool.class.getCanonicalName());
    private String protocol = "https";
    private String host = "doi.crossref.org";
    private int port = 443;
    private String path = "/deposit/";
    private String userName;
    private String userPassword;
    private List<String[]> additionalParameters = new LinkedList<>();
    private Map<String, List<File>> submissionsByType = new HashMap<>();
    private boolean dryRun = false;

    public static void main(String... args) throws Exception {
        CrossrefSubmissionUploadTool tool = new CrossrefSubmissionUploadTool();
        for (int i = 0; i < args.length; i++) {
            if ("--url".equals(args[i])) {
                URL url = new URL(args[i + 1]);
                tool.setProtocol(url.getProtocol());
                tool.setHost(url.getHost());
                tool.setPort(url.getPort());
                tool.setPath(url.getPath());
                i += 1;
            } else if ("--user".equals(args[i])) {
                tool.setUser(args[i + 1], args[i + 2]);
                i += 2;
            } else if ("--metadata".equals(args[i])) {
                for (File file : listFiles(new File(args[i + 1]))) {
                    tool.addMetadata(file);
                }
                i += 1;
            } else if ("--resources".equals(args[i])) {
                for (File file : listFiles(new File(args[i + 1]))) {
                    tool.addResource(file);
                }
                i += 1;
            } else if ("--transfers".equals(args[i])) {
                for (File file : listFiles(new File(args[i + 1]))) {
                    tool.addTransfer(file);
                }
                i += 1;
            } else if ("--handles".equals(args[i])) {
                for (File file : listFiles(new File(args[i + 1]))) {
                    tool.addHandle(file);
                }
                i += 1;
            } else if ("--conflicts".equals(args[i])) {
                for (File file : listFiles(new File(args[i + 1]))) {
                    tool.addConflict(file);
                }
                i += 1;
            } else if ("--query".equals(args[i]) || "--queries".equals(args[i])) {
                for (File file : listFiles(new File(args[i + 1]))) {
                    tool.addQuery(file);
                }
                i += 1;
            } else if ("--host".equals(args[i])) {
                tool.setHost(args[i + 1]);
                i += 1;
            } else if ("--dry-run".equals(args[i])) {
                tool.setDryRun(true);
            } else if ("--version".equals(args[i])) {
                System.out.printf("%s 1.1.0\n", CrossrefSubmissionUploadTool.class.getCanonicalName());
                System.exit(0);
            } else if ("--usage".equals(args[i])) {
                tool.setUsage(args[i + 1]);
                i += 1;
            } else if ("--p".equals(args[i])) {
                tool.addAdditionalParameter(args[i + 1], args[i + 2]);
                i += 2;
            } else {
                if (args[i].startsWith("-")) {
                    if (!"--help".equals(args[i])) {
                        System.out.printf("error: unknown command line argument \"%s\"\n", args[i]);
                    }
                    System.out.printf("%s"
                            + "  --user name password \n"
                            + "  --metadata ( file | directory ) \n"
                            + "  --conflicts ( file | directory ) \n"
                            + "  --transfers ( file | directory ) \n"
                            + "  --resources ( file | directory ) \n"
                            + "  --handles ( file | directory ) \n"
                            + "  --query ( file | directory ) \n"
                            + "  --host host \n"
                            + "  --url deposit-url \n"
                            + "  --dry-run \n"
                            + "  --help\n", tool.usage);
                    System.exit(1);
                }
            }
        }
        tool.execute();
    }

    public void addAdditionalParameter(String name, String value) {
        this.additionalParameters.add(new String[]{name, value});
    }

    public void setUsage(String usage) {
        this.usage = usage;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setUser(String userName, String userPassword) {
        this.userName = userName;
        this.userPassword = userPassword;
    }

    public void addSubmisson(String type, File file) {
        List<File> files = submissionsByType.get(type);
        if (files == null) {
            files = new ArrayList<>();
            submissionsByType.put(type, files);
        }
        files.add(file);
    }

    public void addMetadata(File file) {
        addSubmisson("doMDUpload", file);
    }

    public void addResource(File file) {
        addSubmisson("doDOICitUpload", file);
    }

    public void addTransfer(File file) {
        addSubmisson("doTransferDOIsUpload", file);
    }

    public void addHandle(File file) {
        addSubmisson("doHandleSynchUpload", file);
    }

    public void addConflict(File file) {
        addSubmisson("doMDUpload", file);
    }

    public void addQuery(File file) {
        addSubmisson("doQueryUpload", file);
    }

    public void setDryRun(boolean dryRun) {
        this.dryRun = dryRun;
    }

    public void execute() throws Exception {
        if (dryRun) {
            log(Level.INFO, "making a dry run");
        }

        if (userName == null || userPassword == null) {
            log(Level.ERROR, "user name and password must be given");
            return;
        }

        log(Level.INFO, "uploading to {0}://{1}:{2,number,####}{3}", protocol, host, port, path);

        HttpClient client = new HttpClient();

        String url = new SimpleUrlBuilder()
                .setProtocol(protocol)
                .setHost(host)
                .setPort(port)
                .setPath(path)
                .build();

        for (String type : submissionsByType.keySet()) {
            for (File file : submissionsByType.get(type)) {
                log(Level.INFO, "uploading submission: file={0} ({1})", file, type);

                if (!dryRun) {
                    Part[] parts = new Part[4 + additionalParameters.size()];
                    parts[0] = new StringPart("operation", type, "UTF-8");
                    parts[1] = new StringPart("usr", userName, "UTF-8");
                    parts[2] = new StringPart("pwd", userPassword, "UTF-8");
                    parts[3] = new FilePart("mdFile", file, "text/xml", "UTF-8");
                    for (int i = 4, l = parts.length; i < l; i++) {
                        String[] additionalParameter = additionalParameters.get(i - 4);
                        parts[i] = new StringPart(additionalParameter[0], additionalParameter[1], "UTF-8");
                    }

                    PostMethod method = new PostMethod(url);
                    try {
                        method.setRequestEntity(new MultipartRequestEntity(parts, method.getParams()));

                        int statusCode = client.executeMethod(method);
                        switch (statusCode) {
                            case 200: {
                                log(Level.INFO, "uploaded submission: file={0}", file);
                                String result = method.getResponseBodyAsString();
                                if (result != null && result.trim().length() > 0) {
                                    System.out.print(result);
                                }
                                break;
                            }
                            case 403: {
                                log(Level.INFO, "unsuccessful submission: file={0}; user={1}", file, userName);
                                String result = method.getResponseBodyAsString();
                                if (result != null && result.trim().length() > 0) {
                                    System.out.print(result);
                                }
                                break;
                            }
                            case 401: {
                                log(Level.INFO, "unauthorized: file={0}; user={1}", file, userName);
                                break;
                            }
                            default: {
                                log(Level.INFO, "unable to upload submission: status={0}; file={1}", statusCode, file);
                            }
                        }
                    }
                    finally {
                        method.releaseConnection();
                    }
                }
            }
        }

        log(Level.INFO, "done");
    }

    private static List<File> listFiles(File element) {
        List<File> files = new LinkedList<>();
        if (element.isDirectory()) {
            for (File f : element.listFiles()) {
                if (f.isFile()) {
                    files.add(f);
                }
            }
        } else {
            files.add(element);
        }
        return files;
    }

    private void log(Level level, Throwable throwable, String message, Object... parameters) {
        log(level, message, parameters);
        throwable.printStackTrace(System.out);
    }

    private void log(Level level, String message, Object... parameters) {
        System.out.print(LOG_DATE_FORMAT.format(new Date()));
        System.out.print(" ");
        System.out.print(level.name());
        System.out.print(" ");
        System.out.println(MessageFormat.format(message, parameters));
    }

    static class SimpleUrlBuilder {

        private static final Map<String, Integer> PROTOCOL_TO_DEFAULT_PORT = new HashMap<>();

        static {
            PROTOCOL_TO_DEFAULT_PORT.put("http", 80);
            PROTOCOL_TO_DEFAULT_PORT.put("https", 443);
            PROTOCOL_TO_DEFAULT_PORT.put("ftp", 20);
        }

        private String protocol = "http";
        private String host = "localhost";
        private int port = 80;
        private File path;
        private Map<String, List<String>> query = new TreeMap<>();
        private String reference;

        private SimpleUrlBuilder() {
            // empty
        }

        public SimpleUrlBuilder(String seedUrl) throws IllegalStateException {
            try {
                URL url = new URL(seedUrl);
                this.protocol = url.getProtocol();
                this.host = url.getHost();
                this.port = url.getPort();
                this.path = new File(url.getPath());
                this.query = parseQuery(url.getQuery(), new HashMap<String, List<String>>());
                this.reference = url.getRef();
            } catch (MalformedURLException e) {
                throw new IllegalStateException("unable to parse url " + seedUrl, e);
            }
        }

        public SimpleUrlBuilder copy() {
            SimpleUrlBuilder b = new SimpleUrlBuilder();
            b.setProtocol(protocol);
            b.setHost(host);
            b.setPort(port);
            b.setPath(path.getAbsolutePath());
            b.setQuery(query);
            b.setReference(reference);
            return b;
        }

        public SimpleUrlBuilder setProtocol(String protocol) {
            this.protocol = protocol;
            return this;
        }

        public SimpleUrlBuilder setHost(String host) {
            this.host = host;
            return this;
        }

        public SimpleUrlBuilder setPort(int port) {
            this.port = port;
            return this;
        }

        public SimpleUrlBuilder setPath(String path) {
            this.path = new File(path);
            return this;
        }

        public SimpleUrlBuilder setQuery(Map<String, List<String>> query) {
            this.query = new HashMap<>(query);
            return this;
        }

        public SimpleUrlBuilder setReference(String reference) {
            this.reference = reference;
            return this;
        }

        public SimpleUrlBuilder addParameter(String name, Object value) {
            List<String> values = query.get(name);
            if (values == null) {
                values = new ArrayList<>();
                query.put(name, values);
            }
            values.add(value.toString());
            return this;
        }

        public SimpleUrlBuilder setParameter(String name, Object value) {
            List<String> values = query.get(name);
            if (values == null) {
                values = new ArrayList<>();
                query.put(name, values);
            } else {
                values.clear();
            }
            values.add(value.toString());
            return this;
        }

        public List<String> findParameter(String name) {
            List<String> values = query.get(name);
            if (values == null) {
                values = Collections.emptyList();
            }
            return values;
        }

        public SimpleUrlBuilder addPath(Object... parts) {
            for (Object part : parts) {
                this.path = new File(this.path, part.toString());
            }
            return this;
        }

        public String build() {
            StringBuilder u = new StringBuilder();
            u.append(protocol);
            u.append("://");
            u.append(host);
            if (port != -1 && (!PROTOCOL_TO_DEFAULT_PORT.containsKey(protocol) || PROTOCOL_TO_DEFAULT_PORT.get(protocol) != port)) {
                u.append(":").append(port);
            }
            u.append(path == null ? "/" : path.getAbsolutePath());
            if (!query.isEmpty()) {
                u.append("?");
                boolean notFirstParmater = false;
                for (Map.Entry<String, List<String>> e : query.entrySet()) {
                    String name = encode(e.getKey());
                    for (Object v : e.getValue()) {
                        if (notFirstParmater) {
                            u.append("&");
                        }
                        u.append(name).append("=").append(encode(v.toString()));
                        notFirstParmater = true;
                    }
                }
            }
            if (reference != null) {
                u.append("#").append(reference);
            }
            return u.toString();
        }

        public static Map<String, List<String>> parseQuery(String query, Map<String, List<String>> parameters) {
            if (query != null) {
                StringTokenizer pairs = new StringTokenizer(query, "&");
                while (pairs.hasMoreTokens()) {
                    String pair = pairs.nextToken();
                    StringTokenizer parts = new StringTokenizer(pair, "=");
                    String name = decode(parts.nextToken());
                    String value = parts.hasMoreTokens() ? decode(parts.nextToken()) : "";
                    List<String> values = parameters.get(name);
                    if (values == null) {
                        values = new LinkedList<>();
                        parameters.put(name, values);
                    }
                    values.add(value);
                }
            }
            return parameters;
        }

        public static String decode(String input) {
            try {
                return URLDecoder.decode(input, "ISO-8859-1");
            } catch (UnsupportedEncodingException e) {
                throw new IllegalStateException(e);
            }
        }

        public static String encode(String input) {
            try {
                return URLEncoder.encode(input, "ISO-8859-1");
            } catch (UnsupportedEncodingException e) {
                throw new IllegalStateException(e);
            }
        }
    }

}

// END
